﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace DropDemBalls
{
    public class CanvasManager : MonoBehaviour
    {

        public Button cycleTypeButton;
        public Button restartButton;
        public Button quitButton;

        public Text gameOverText;
        public Text highScoreText;
        public Text scoreText;

        // Use this for initialization
        void Start()
        {
            // Make sure they're not visible at the start
            gameOverText.gameObject.SetActive(false);
            restartButton.gameObject.SetActive(false);

            // Then program in what they're supposed to do when they run
            cycleTypeButton.onClick.AddListener(PlayerManager.instance.NextBlockType);
            restartButton.onClick.AddListener(GameManager.instance.RestartGame);
            quitButton.onClick.AddListener(GameManager.instance.QuitGame);

            // Finally prepare for gameover
            GameManager.instance.OnGameOver += GameManager_OnGameOver;
        }

        private void GameManager_OnGameOver()
        {
            // They should be visible now
            gameOverText.gameObject.SetActive(true);
            restartButton.gameObject.SetActive(true);

            // Gotta clean up after yourself
            // For some reason these events don't go away even if they
            // object they're a part of is destroyed
            // TODO: Figure out why that thing above
            GameManager.instance.OnGameOver -= GameManager_OnGameOver;
        }

        // Update is called once per frame
        void Update()
        {
            highScoreText.text = "High Score: " + PlayerPrefs.GetInt("HighScore");
            scoreText.text = "Score: " + GameManager.instance.score;
        }
    }
}
