﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DropDemBalls
{
    [AddComponentMenu("DropDemBalls/Obstacle Pool")]
    public class ObstacleBlockPool : MonoBehaviour
    {
        [Header("Obstacles")]
        public GameObject[] blocks;
        public int numberInitialObstacles;

        [Header("Transforms")]
        public GameObject spawnPoint;

        GameObject player;
        float playerOffset;

        [Header("Difficulty")]
        public float spacing = 15.0f;

        List<GameObject> availableObstacles;
        List<GameObject> activeObstacles;

        Vector3 lastSpawnPosition;
        Vector3 lastPoolPosition;

        // Use this for initialization
        void Start()
        {
            player = PlayerManager.instance.player.gameObject;
            playerOffset = transform.position.y - player.transform.position.y;

            availableObstacles = new List<GameObject>();
            activeObstacles = new List<GameObject>();

            for(int i = 0 ; i < numberInitialObstacles ; i++)
            {
                // Add a bunch of randomly chosen blocks to the list
                availableObstacles.Add(GenerateNewBlock());
            }

            // Keep track of where we are so we can know when to spawn
            lastSpawnPosition = spawnPoint.transform.position;
            lastPoolPosition = transform.position;

            // Preseed
            int size = availableObstacles.Count;
            for(int i = 0; i < size; i++)
            {
                GetRandomAvailableBlock();
            }
        }

        // Update is called once per frame
        void Update()
        {
            // When we've gone further than our spacing, spawn and update
            if((lastPoolPosition.y - spacing) > transform.position.y)
            {
                GetRandomAvailableBlock(); // Grab a new one to spawn
                lastPoolPosition.y -= spacing; // Update the last position when spawning
            }
        }

        void FixedUpdate()
        {
            Vector3 newPosition = transform.position;
            newPosition.y = player.transform.position.y + playerOffset;
            transform.position = newPosition;
        }

        GameObject GenerateNewBlock()
        {
            GameObject block = Instantiate(blocks[Mathf.FloorToInt(Random.Range(0, blocks.Length))]);
            block.SetActive(false);
            return block;
        }

        GameObject GetRandomAvailableBlock()
        {
            if(availableObstacles.Count <= 0)
            {
                availableObstacles.Add(GenerateNewBlock());
            }
            // Grab the index (after making sure one exists
            int idx = Mathf.FloorToInt(Random.Range(0, availableObstacles.Count));
            GameObject block = availableObstacles[idx];
            availableObstacles.Remove(block); // Remove it from the available list
            activeObstacles.Add(block); // Add it to the active list
            block.SetActive(true); // Make sure they're actually active
            block.GetComponent<ObstacleBlock>().RandomBlockType(); // Randomize its type
            PositionBlock(block);
            return block;
        }

        void PositionBlock(GameObject block)
        {
            block.transform.position = lastSpawnPosition;
            lastSpawnPosition.y -= spacing; // Make sure we're going down!
        }

        void OnTriggerExit2D(Collider2D other)
        {
            if (other.CompareTag("Obstacle"))
            {
                GameObject block = other.gameObject;
                block.SetActive(false);
                activeObstacles.Remove(block);
                availableObstacles.Add(block);
            }
        }
    }
}
