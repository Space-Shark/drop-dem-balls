﻿using UnityEngine;
using System.Collections;

namespace DropDemBalls
{
    [RequireComponent(typeof(Collider2D),typeof(MeshRenderer))]
    public abstract class BaseBlock : MonoBehaviour
    {
        /// <summary>
        /// Base object from which all other block
        /// objects will be pulled from.
        /// Contains list of possible Block Types the object
        /// can pull from.
        /// </summary>
        public BlockType[] types;
        public int idx = 0;
        public BlockType currentType
        {
            get
            {
                // Make sure we're not trying to
                // access something we don't have
                // access to
                idx = idx % types.Length;
                return types[idx];
            }
        }

        MeshRenderer meshRenderer;

        // Use this for initialization
        protected void Start()
        {
            // Grab the mesh renderer
            // This is what we will use to change the colors
            meshRenderer = gameObject.GetComponent<MeshRenderer>();
        }

        // Update is called once per frame
        protected void Update()
        {
            meshRenderer.material = currentType.material;
        }

        public void NextBlockType()
        {
            // Cycle the block types
            // Make sure we don't go over the potential
            // number of items!
            idx = (idx+1) % types.Length;
        }

        public void RandomBlockType()
        {
            // In case we want a random color
            // Once again, making sure we don't go index out of range
            idx = Mathf.RoundToInt(Random.Range(0, types.Length));
        }

        // EDITOR ONLY
        // Attempt at validation
        void OnValidate()
        {
            if(types.Length <= 0)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
