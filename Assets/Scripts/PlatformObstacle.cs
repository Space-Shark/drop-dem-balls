﻿using UnityEngine;
using System.Collections;

namespace DropDemBalls
{
    [AddComponentMenu("DropDemBalls/Platform")]
    public class PlatformObstacle : ObstacleBlock
    {

        // Use this for initialization
        protected new void Start()
        {
            base.Start();
        }

        // Update is called once per frame
        protected new void Update()
        {
            base.Update();
        }
    }
}
