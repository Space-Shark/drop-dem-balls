﻿using UnityEngine;
using System.Collections;

namespace DropDemBalls
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "BlockType",menuName = "DropDemBalls/BlockType")]
    public class BlockType : ScriptableObject
    {
        public enum BlockColor
        {
            RED,
            BLUE,
            GREEN,
            YELLOW
        }
        
        public BlockColor color;
        public Material material;
    }
}