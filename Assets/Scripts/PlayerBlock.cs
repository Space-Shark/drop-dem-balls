﻿using UnityEngine;
using System.Collections;

namespace DropDemBalls
{
    [AddComponentMenu("DropDemBalls/Player")]
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerBlock : BaseBlock
    {
        public float speed = 5.0f;

        Camera mainCamera;
        float cameraOffset;

        void Awake()
        {
            // Make sure there's a player
            if (PlayerManager.instance.player == null)
                PlayerManager.instance.player = this;

            // If we're not the player already, kill ourselves
            if (PlayerManager.instance.player != this)
                Destroy(this);
        }

        protected new void Start()
        {
            base.Start();

            // Make sure the camera offset is set early
            mainCamera = Camera.main;
            cameraOffset = mainCamera.transform.position.y - transform.position.y;
        }

        protected new void Update()
        {
            base.Update();
        }

        void FixedUpdate()
        {
            base.Update();
            // Manually move the block downward
            Vector3 newPosition = transform.position;
            newPosition.y -= speed * Time.deltaTime;
            transform.position = newPosition;
        }

        void LateUpdate()
        {
            // Update the camera to follow the player
            Vector3 newCameraPosition = mainCamera.transform.position;
            newCameraPosition.y = transform.position.y + cameraOffset;
            mainCamera.transform.position = newCameraPosition;
        }
    }
}
