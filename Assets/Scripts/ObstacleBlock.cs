﻿using UnityEngine;
using System.Collections;

namespace DropDemBalls
{
    public abstract class ObstacleBlock : BaseBlock
    {

        // Use this for initialization
        protected new void Start()
        {
            base.Start();
        }

        // Update is called once per frame
        protected new void Update()
        {
            base.Update();
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                PlayerBlock player = other.GetComponent<PlayerBlock>();
                if(player !=  null)
                {
                    if(player.currentType != currentType)
                    {
                        GameManager.instance.CallGameOver();
                    } else
                    {
                        GameManager.instance.IncScore();
                    }
                }
            }
        }
    }
}
