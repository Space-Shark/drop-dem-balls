﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

namespace DropDemBalls
{
    [AddComponentMenu("DropDemBalls/Game Manager")]
    public class GameManager : MonoBehaviour
    {
        private static GameManager _instance;
        public static GameManager instance
        {
            get
            {
                // If doesn't exist, make a new gameobject
                if (_instance == null)
                {
                    GameObject empty = new GameObject("_Managers");
                    _instance = empty.AddComponent<GameManager>();
                }
                return _instance;
            }
        }

        int _score = 0;
        public int score
        {
            get
            {
                return _score;
            }
            protected set
            {
                _score = value;
            }
        }
        public int highScore
        {
            get
            {
                return PlayerPrefs.GetInt("HighScore");
            }
            protected set
            {
                PlayerPrefs.SetInt("HighScore", value);
            }
        }

        void Awake()
        {
            // See if the singleton exists
            if (_instance == null)
                _instance = this;

            // See if this is the singleton
            if (_instance != this)
                Destroy(this);

            // Make sure this doesn't go away between loads
            DontDestroyOnLoad(_instance);

            SceneManager.sceneLoaded += SceneManager_sceneLoaded;
        }

        private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
        {
            score = 0;
            CallGameStart();
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void IncScore()
        {
            score++;
            if (score > highScore)
                highScore = score;
        }

        public delegate void GameStart();
        public event GameStart OnGameStart;
        public void CallGameStart()
        {
            if(OnGameStart != null)
                OnGameStart();
        }

        public delegate void GameOver();
        public event GameOver OnGameOver;
        public void CallGameOver()
        {
            if(OnGameOver != null)
                OnGameOver();
        }

        public void RestartGame()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}
