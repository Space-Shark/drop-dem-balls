﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace DropDemBalls
{
    public class PlayerManager : MonoBehaviour
    {
        private static PlayerManager _instance;
        public static PlayerManager instance
        {
            get
            {
                // If doesn't exist, make a new gameobject
                if (_instance == null)
                {
                    GameObject empty = new GameObject("_Managers");
                    _instance = empty.AddComponent<PlayerManager>();
                }
                return _instance;
            }
        }

        public PlayerBlock player;
        public float startSpeed = 5.0f;
        public float speedIncreaseRate = 1.0f;
        float speed;
        bool playing = true;

        void Awake()
        {
            // See if the singleton exists
            if (_instance == null)
                _instance = this;

            // See if this is the singleton
            if (_instance != this)
                Destroy(this);

            // Make sure this doesn't go away between loads
            DontDestroyOnLoad(_instance);
        }

        // Use this for initialization
        void Start()
        {
            GameManager.instance.OnGameStart += GameManager_OnGameStart;
            GameManager.instance.OnGameOver += GameManager_OnGameOver;

            GameManager_OnGameStart();
        }

        private void GameManager_OnGameStart()
        {
            speed = startSpeed;
            playing = true;
        }

        private void GameManager_OnGameOver()
        {
            playing = false;
        }

        // Update is called once per frame
        void Update()
        {

        }

        // Controlling the "physics" so use FixedUpdates
        void FixedUpdate()
        {
            if(playing)
            {
                player.speed = speed;
                speed += (speedIncreaseRate * Time.fixedDeltaTime);
            } else
            {
                player.speed = 0;
            }
        }

        public void NextBlockType()
        {
            player.NextBlockType();
        }
    }
}
